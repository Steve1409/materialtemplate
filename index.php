<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <?php wp_head();?>
  </head>
  <body>

    <div id="wrapper" class="animate">
      <nav class="navbar header-top fixed-top navbar-expand-lg  navbar-light bg-light">
        <span class="leftmenutrigger" id="left"><i class="fa fa-bars" aria-hidden="true"></i></span>
        <a class="navbar-brand" href="#">Bootstrap 4 navigation</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText"
          aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarText">
          <ul class="navbar-nav animate side-nav">
            <li class="nav-item">
              <a class="nav-link" href="#">Home
                <span class="sr-only">(current)</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Side Menu Items</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Pricing</a>
            </li>
          </ul>
          <!-- <ul class="navbar-nav ml-md-auto d-md-flex">
            <li class="nav-item">
              <a class="nav-link" href="#">Home
                <span class="sr-only">(current)</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Top Menu Items</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Pricing</a>
            </li>
          </ul> -->
        </div>
      </nav>


      <div class="container-fluid" id="fullPage">
        <div id="btn">
          <button href="#" class="btn btn-lg btn-default">Start Survey &#9658;</button>
        </div>
      </div>
    </div>
    <?php wp_footer();?>
  </body>
</html>
