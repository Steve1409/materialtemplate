<?php

// remove admin bar
add_filter('show_admin_bar', '__return_false');
function styles() {
  wp_enqueue_style('mainStyle', get_stylesheet_uri());
  wp_enqueue_style( 'fontAwesome', 'https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css',array(), $ver = false,'all' );
  wp_enqueue_style('bootstrap','//maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css');
  wp_enqueue_script( 'jquery', 'https://code.jquery.com/jquery-3.3.1.slim.min.js',array(), false, true );
  wp_enqueue_script('mainJs', get_template_directory_uri(). '/js/app.js', array('jquery'), false, true);
  wp_enqueue_script('bootJs', 'https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js', array('jquery'), false,true);
}
add_action('wp_enqueue_scripts','styles');
